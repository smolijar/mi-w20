# RESTfull - Asynchronous operation

## Task

Design and implement simple service - remove customer from your database.

```
DELETE /customer/{id}
```

Assume that this operation requires confirmation by human. It should not be implemented as synchronous operation. Design and implement this operation in an asynchronous manner. You can simulate the confirmation as delay 10s or implement another service to confirm deletion.

Use [http://nodejs.org](http://nodejs.org/) and [http module](http://nodejs.org/api/http.html).

## Solution

On delete, **202 Accepted** is returned, with link to deletion process, which progress increases in time. When done, customer is deleted.

### Example

#### List customers

```
GET /customer/
```

```json
[
  {
    "id": 1,
    "name": "Richard Lawson"
  },
  {
    "id": 2,
    "name": "Diana Thompson"
  },
  {
    "id": 3,
    "name": "Earl Nguyen"
  },
  {
    "id": 4,
    "name": "Christopher Coleman"
  },
  {
    "id": 5,
    "name": "Angela Simmons"
  }
]
```

#### Delete cutomer

Returns 202 and link to watch the progress.

```
DELETE /customer/3
```

```json
Delete request accepted and in progress. Check progress at: http://localhost:8080/process/1
```

#### Check progress 1

Returns pid, progress (percentage) and related customer.

```
GET /process/1
```

```json
{
  "id": 1,
  "progress": 11,
  "customer": 3
}
```

#### List customers intact

Customers are still intact

```
GET /customer/
```

```json
[
  {
    "id": 1,
    "name": "Richard Lawson"
  },
  {
    "id": 2,
    "name": "Diana Thompson"
  },
  {
    "id": 3,
    "name": "Earl Nguyen"
  },
  {
    "id": 4,
    "name": "Christopher Coleman"
  },
  {
    "id": 5,
    "name": "Angela Simmons"
  }
]
```

#### Check progress 2

Progress has been updated

```
GET /process/1
```

```json
{
  "id": 1,
  "progress": 72,
  "customer": 3
}
```

#### Check progress 3

Porgress is complete (process remains after completion)

```
GET /process/1
```

```json
{
  "id": 1,
  "progress": 100,
  "customer": 3
}
```

#### List customers deleted

Custemer 3 is missing as expected.

```
GET /customer/
```

```json
[
  {
    "id": 1,
    "name": "Richard Lawson"
  },
  {
    "id": 2,
    "name": "Diana Thompson"
  },
  {
    "id": 4,
    "name": "Christopher Coleman"
  },
  {
    "id": 5,
    "name": "Angela Simmons"
  }
]
```

### Implementation

Available at [Gitlab](https://gitlab.fit.cvut.cz/smolijar/mi-w20/tree/master/Homework/4).

```javascript
const http = require('http');

const customer = [...];
const process = [];
const data = {customer, process};

function processDelete(id) {
    let pindex = data.process.map(v => v.id).indexOf(id);
    if(data.process[pindex].progress >= 100) {
        data.customer = data.customer.filter(c => c.id !== data.process[pindex].customer);
    } else {
        data.process[pindex].progress += 1;
        setTimeout(() => {
            processDelete(id);
        }, 300);
    }
}
function handle(req, res) {
    let [collectionName, id] = req.url.replace(/^\/|\/$/g, '').split('/');
    id = + id;
    const resource = data[collectionName].find(v => v.id === id);
    if(data[collectionName] && (!id || resource)) {
        if(req.method === 'GET') {
            res.writeHead(200, {'Content-Type': 'application/json'});
            if(id) {
                res.end(JSON.stringify(resource));
            } else {
                res.end(JSON.stringify(data[collectionName]));
            }
        }
        else if(req.method === 'DELETE' && collectionName === 'customer' && resource) {
            pid = data['process'].reduce((acc, val) => Math.max(acc, val.id), 1);
            data['process'].push({id: pid, progress: 0.0, customer: id});
            processDelete(pid);
            res.writeHead(202);
            res.end(`Delete request accepted and in progress. Check progress at: http://${req.headers.host}/process/${pid}`);
        }
    } else {
        res.writeHead(404);
        res.end('Not found');
    }
}

http.createServer((req, res) => {
    req.body = '';
    req.on('data', (chunk) => req.body += chunk);
    req.on('end', () => {
        handle(req, res);
    });
}).listen(8080);
```

