const http = require('http');

const customer = [{
    "id": 1,
    "name": "Richard Lawson"
}, {
    "id": 2,
    "name": "Diana Thompson"
}, {
    "id": 3,
    "name": "Earl Nguyen"
}, {
    "id": 4,
    "name": "Christopher Coleman"
}, {
    "id": 5,
    "name": "Angela Simmons"
}];
const process = [];
const data = {customer, process};

function processDelete(id) {
    let pindex = data.process.map(v => v.id).indexOf(id);
    if(data.process[pindex].progress >= 100) {
        data.customer = data.customer.filter(c => c.id !== data.process[pindex].customer);
    } else {
        data.process[pindex].progress += 1;
        setTimeout(() => {
            processDelete(id);
        }, 300);
    }
}
function handle(req, res) {
    let [collectionName, id] = req.url.replace(/^\/|\/$/g, '').split('/');
    id = + id;
    const resource = data[collectionName].find(v => v.id === id);
    if(data[collectionName] && (!id || resource)) {
        if(req.method === 'GET') {
            res.writeHead(200, {'Content-Type': 'application/json'});
            if(id) {
                res.end(JSON.stringify(resource));
            } else {
                res.end(JSON.stringify(data[collectionName]));
            }
        }
        else if(req.method === 'DELETE' && collectionName === 'customer' && resource) {
            pid = data['process'].reduce((acc, val) => Math.max(acc, val.id), 1);
            data['process'].push({id: pid, progress: 0.0, customer: id});
            processDelete(pid);
            res.writeHead(202);
            res.end(`Delete request accepted and in progress. Check progress at: http://${req.headers.host}/process/${pid}`);
        }
    } else {
        res.writeHead(404);
        res.end('Not found');
    }
}

http.createServer((req, res) => {
    req.body = '';
    req.on('data', (chunk) => req.body += chunk);
    req.on('end', () => {
        handle(req, res);
    });
}).listen(8080);