# RESTfull - Resources, URIs, operations

## Task

A company runs an application for cars registration and provides RESTful services to its customers. Resources have a base uri of [http://car-registry.org](http://car-registry.org/)

The data model contains following entities: place ← owner → car → plate.

The services should cover all CRUD operations for each entity/resource.

## Solution

### Endpoints

#### Lists / Detials

```
GET /places/[<id>/]
GET /owners/[<id>/]
GET /cars/[<id>/]
GET /plates/[<id>/]
```

#### Filters

Only those, where given *attribute* contains given *value*.

```
GET /places/?attr=val (eg. /owners/?email=@kickstarter.com)
GET /owners/?attr=val
GET /cars/?attr=val
GET /plates/?attr=val
```

#### Relations

All second sources in relation with the first source

```
GET /places/<id>/owners/
GET /owners/<id>/cars/
GET /cars/<id>/owners/
GET /cars/<id>/plates/
GET /plates/<id>/cars/
```

#### Creation

```
POST /places/
POST /owners/
POST /cars/
POST /plates/
```

#### Update

Update resource by id, or whole collection

```
PUT /places/[<id>/]
PUT /owners/[<id>/]
PUT /cars/[<id>/]
PUT /plates/[<id>/]
```

#### Delete

Delete resource by id.

```
DELETE /places/<id>/
DELETE /owners/<id>/
DELETE /cars/<id>/
DELETE /plates/<id>/
```

### Implementation

Available at [Gitlab](https://gitlab.fit.cvut.cz/smolijar/mi-w20/tree/master/Homework/3).

```javascript
const http = require('http');

const places = [...];
const owners = [...];
const cars = [...];
const plates = [...];

function singular(str) {...}
const storage = {places, owners, cars, plates};
const allowedRelations = {...};

function handle(req, res) {
    const path = req.url.replace(/^\/|\/$/g, '').split('/');
    let sourceName, sourceId, relatedEntityName;
    if(path.length < 1 || path.length > 3) {
        res.writeHead(404);
        res.end('Not found');
        return;
    }

    const qeury = req.url.split('?')[1];
    let attr, val;
    if(qeury) {
        [attr, val] = qeury.split('=');
    }

    sourceName = path[0];
    if(path.length > 1) {
        sourceId = +path[1];
        if(path.length > 2) {
            relatedEntityName = path[2];
        }
    }
    if(storage[sourceName]) {
        let repo = storage[sourceName], resource;
        if(sourceId) {
            resource = repo.find((v) => v.id === sourceId);
            if((sourceId && !resource)) {
                res.writeHead(404);
                res.end('Relation not allowed');
            }
        }
        if(req.method === 'GET') {
            res.writeHead(200, {'Content-Type': 'application/json'});
            if(relatedEntityName && storage[relatedEntityName]) {
                res.end(JSON.stringify(storage[relatedEntityName].filter(
                    (v) => v[`${singular(sourceName)}_id`] === sourceId)
                ));
            }
            else if(resource) {
                res.end(JSON.stringify(resource));
            } else {
                if(attr && val) {
                    res.end(JSON.stringify(repo.filter(v => v[attr].includes(val))));
                } else {
                    res.end(JSON.stringify(repo));
                }

                res.end(JSON.stringify(repo));
            }
        } else if(req.method === 'POST') {
            if(resource || relatedEntityName) {
                res.writeHead(405);
                res.end('Method not allowed');
            } else {
                sourceId = repo.reduce((acc, val) => Math.max(acc, val.id), 0);
                repo.push(Object.assign({}, JSON.parse(req.body), {id: sourceId + 1}));
                storage[sourceName] = repo;
                const location = `http://${req.headers.host}${req.url}${sourceId}`;
                res.writeHead(201, {'Location': location});
                res.end(`Created at ${location}`);
            }
        } else if(req.method === 'DELETE') {
            if(!resource || relatedEntityName) {
                res.writeHead(405);
                res.end('Method not allowed');
            } else {
                storage[sourceName] = repo.filter((v) => v.id !== sourceId);
                res.writeHead(200);
                res.end('Deleted');
            }
        } else if(req.method === 'PUT') {
            if(relatedEntityName) {
                res.writeHead(405);
                res.end('Method not allowed');
            } else {
                if(resource) {
                    repo = repo.filter((v) => v.id != sourceId);
                    repo.push(Object.assign({}, resource, JSON.parse(req.body), {sourceId}));
                } else {
                    repo = JSON.stringify(req.body);
                }
                storage[sourceName] = repo.sort((a, b) => a.sourceId - b.sourceId);
                res.writeHead(200);
                res.end('Updated');
            }
        }
    }
}

http.createServer((req, res) => {
    req.body = '';
    req.on('data', (chunk) => req.body += chunk);
    req.on('end', () => {
        handle(req, res);
    });
}).listen(8080);
```

## Data

Created at https://www.mockaroo.com/

```sh
const places = [{
    "id": 1,
    "latitude": "-8.7073",
    "longitude": "121.2809"
}, {
    "id": 2,
    "latitude": "33.27333",
    "longitude": "35.19389"
}];
const owners = [{
    "id": 1,
    "name": "Rebecca Richards",
    "email": "rrichards0@bravesites.com",
    "street": "74 Park Meadow Court",
    "place_id": 1,
    "car_id": 1
}, {
    "id": 2,
    "name": "Judy Reynolds",
    "email": "jreynolds1@kickstarter.com",
    "street": "241 Sachs Way",
    "place_id": 1,
    "car_id": 2
}, {
    "id": 3,
    "name": "Wayne Stanley",
    "email": "wstanley2@xing.com",
    "street": "9 Melvin Center",
    "place_id": 2,
    "car_id": 3
}, {
    "id": 4,
    "name": "Bruce Wheeler",
    "email": "bwheeler3@hubpages.com",
    "street": "3255 Stuart Street",
    "place_id": 2,
    "car_id": 4
}, {
    "id": 5,
    "name": "Ronald Franklin",
    "email": "rfranklin4@ted.com",
    "street": "9522 Schiller Court",
    "place_id": 1,
    "car_id": 5
}];
const cars = [{
    "id": 1,
    "color": "Purple",
    "seats": 4,
    "owner_id": 1,
    "plate_id": 1
}, {
    "id": 2,
    "color": "Khaki",
    "seats": 2,
    "owner_id": 2,
    "plate_id": 2
}, {
    "id": 3,
    "color": "Puce",
    "seats": 4,
    "owner_id": 3,
    "plate_id": 3
}, {
    "id": 4,
    "color": "Purple",
    "seats": 2,
    "owner_id": 4,
    "plate_id": 4
}, {
    "id": 5,
    "color": "Violet",
    "seats": 4,
    "owner_id": 4,
    "plate_id": 4
}];
const plates = [{
    "id": 1,
    "country": "CN",
    "plate": "192-85-2779",
    "car_id": 1
}, {
    "id": 2,
    "country": "BY",
    "plate": "900-18-1338",
    "car_id": 2
}, {
    "id": 3,
    "country": "KP",
    "plate": "229-78-0731",
    "car_id": 3
}, {
    "id": 4,
    "country": "FR",
    "plate": "189-11-6777",
    "car_id": 4
}, {
    "id": 5,
    "country": "CA",
    "plate": "807-90-8761",
    "car_id": 5
}];
```
