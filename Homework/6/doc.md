# AtomPub

## Task

Following xml provides information about one photo album and its photos.

Rewrite the data in the xml to the Atom Syndication Format. When it is necessary, define your own namespace for elements that the standard Atom Syndication Format specification does not define. Use all mandatory fields and also all data/fields in the following xml.

```xml
<album>
    <title>Trip to Prague</title>
    <person>Peter Novak</person> <!-- author -->
    <photo>
        <title>Prague Castle</title> <!-- name of photo -->
        <format>JPG</format> <!-- format of photo -->
        <url>http://store.it/users/432/albums/635/photos/prague-castle.jpg</url>
        <stars>5</stars> <!-- rating of photo -->
    </photo>
    <photo>
        ...
    </photo>
</album>
```

## Solution

Available at [gitlab](https://gitlab.fit.cvut.cz/smolijar/mi-w20/tree/master/Homework/6)

```xml
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns:rating="https://schema.org/Rating"
      xmlns="http://www.w3.org/2005/Atom">
    <title>Trip to Prague</title>
    <id>urn:uuid:1225c695-cfb8-4ebb-aaaa-85f78ac957ba</id>
    <updated>2003-12-13T18:30:04Z</updated>
    <author>
        <name>Peter Novak</name>
    </author>
    <entry href="http://store.it/users/432/albums/635/">
        <title>Prague Castle</title>
        <id>urn:uuid:1225c695-cfb8-4ebb-aaaa-80da344efa6a</id>
        <updated>2003-12-13T18:30:02Z</updated>
        <content type="image/jpeg" src="http://store.it/users/432/albums/635/photos/prague-castle.jpg" />
        <link href="http://store.it/users/432/albums/635/photos/prague-castle.jpg" />
        <rating:ratingValue>5</rating:ratingValue>
    </entry>
    <entry>
        ...
    </entry>
</feed>
```

