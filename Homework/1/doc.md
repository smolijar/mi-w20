# Apps Script - RecipePuppy

## Task

- In new document/sheet create GUI (HTML Service) with following 2 text fields
  - the first field will be used for list of required ingredients for a recipe separated with comma, e.g. `onions,garlic`
  - in the second text field will be the name of the food, e.g. `pizza`
- After sending the form, process the data with the API [Recipe Puppy](http://www.recipepuppy.com/about/api/)
  - in the table/spreadsheet write down the name of the recipe, ingredients, the url and the image url

## Solution

### App script

```javascript
function onOpen() {
    var html = HtmlService.createHtmlOutputFromFile('index');
    SpreadsheetApp.getUi()
        .showModalDialog(html, 'Recipe Puppy');
}

function fetchPuppyRecieps(ingredients, meal) {
    function fetchJson(url) {
        function fetch(url) {
            return UrlFetchApp.fetch(url);
        }
        var response = fetch(url);
        return JSON.parse(response.getContentText());
    }
    var url = 'http://www.recipepuppy.com/api/?i=' + ingredients + '&q=' + meal;
    return fetchJson(url).results;
}

function writePuppyResults(reciepts) {
    var doc = SpreadsheetApp.getActiveSpreadsheet();
    doc.getRange('a1').setValue('Title').setFontWeight('bold');
    doc.getRange('b1').setValue('Ingredients').setFontWeight('bold');
    doc.getRange('c1').setValue('Link').setFontWeight('bold');
    doc.getRange('d1').setValue('Thumbnail').setFontWeight('bold');
    reciepts.forEach(function(r, i) {
        doc.getRange('a' + (i + 2)).setValue(r.title);
        doc.getRange('b' + (i + 2)).setValue(r.ingredients);
        doc.getRange('c' + (i + 2)).setValue(r.href);
        doc.getRange('d' + (i + 2)).setValue(r.thumbnail);
    })
}

function processForm(formObject) {
    writePuppyResults(fetchPuppyRecieps(formObject.ingredients, formObject.meal));
}

```

### HTML form

```html
<!DOCTYPE html>
<html>
  <head>
    <base target="_top">
    <script>
      // Prevent forms from submitting.
      function preventFormSubmit() {
        var forms = document.querySelectorAll('form');
        for (var i = 0; i < forms.length; i++) {
          forms[i].addEventListener('submit', function(event) {
            event.preventDefault();
          });
        }
      }
      window.addEventListener('load', preventFormSubmit);
      function handleFormSubmit(formObject) {
        google.script.run.processForm(formObject);
        google.script.host.close()
      }
    </script>
  </head>
  <body>
    <form id="puppy" onsubmit="handleFormSubmit(this)">
      <label> Ingredients
      <input name="ingredients" type="text" />
      </label> <br> <br>
      <label> Meal
      <input name="meal" type="text" />
      </label> <br> <br>
      <input type="submit" value="Submit" />
    </form>
 </body>
</html>
```

## Shared Link

[Link](https://docs.google.com/spreadsheets/d/11nKlST4A6ZJjNfYs154yq0y4iKhkFHAX3IQSnV-5U2E/edit?usp=sharing)

## Screenshots

![](form.png)

![](result.png)