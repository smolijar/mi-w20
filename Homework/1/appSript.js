function onOpen() {
    var html = HtmlService.createHtmlOutputFromFile('index');
    SpreadsheetApp.getUi()
        .showModalDialog(html, 'Recipe Puppy');
}

function fetchPuppyRecieps(ingredients, meal) {
    function fetchJson(url) {
        function fetch(url) {
            return UrlFetchApp.fetch(url);
        }
        var response = fetch(url);
        return JSON.parse(response.getContentText());
    }
    var url = 'http://www.recipepuppy.com/api/?i=' + ingredients + '&q=' + meal;
    return fetchJson(url).results;
}

function writePuppyResults(reciepts) {
    var doc = SpreadsheetApp.getActiveSpreadsheet();
    doc.getRange('a1').setValue('Title').setFontWeight('bold');
    doc.getRange('b1').setValue('Ingredients').setFontWeight('bold');
    doc.getRange('c1').setValue('Link').setFontWeight('bold');
    doc.getRange('d1').setValue('Thumbnail').setFontWeight('bold');
    reciepts.forEach(function(r, i) {
        doc.getRange('a' + (i + 2)).setValue(r.title);
        doc.getRange('b' + (i + 2)).setValue(r.ingredients);
        doc.getRange('c' + (i + 2)).setValue(r.href);
        doc.getRange('d' + (i + 2)).setValue(r.thumbnail);
    })
}

function testPuppy() {
    writePuppyResults(fetchPuppyRecieps('onions,garlic', 'omelet'));
}

function processForm(formObject) {
    writePuppyResults(fetchPuppyRecieps(formObject.ingredients, formObject.meal));
}
