# Server-Sent Events

## Task

Design and implement simple SSE server in Node.js and simple client in HTML/JavaScript.

- The client connects to server and displays all incoming messages
- The server sends every 2 seconds one message - e.g. one line of text file.

Use [http://nodejs.org](http://nodejs.org/) and [http module](http://nodejs.org/api/http.html).

## Solution

Codes available at [github](https://gitlab.fit.cvut.cz/smolijar/mi-w20/tree/master/Homework/9)

### Server

```javascript
const http = require('http');
const fs = require('fs');

const logger = console;
const port = 3000;
const server = http.createServer();

const text = fs.readFileSync('./text.txt', 'utf8').split('\n');

server.on('request', (req, res) => {

    switch (true) {
        case req.url === '/sse':
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Content-Type', 'text/event-stream');
            res.setHeader('Cache-Control', 'no-cache');
            const t = 2 * 1000;
            let line = 0;
            let finish = null;
            const interval = setInterval(() => {
                if (line === text.length - 1) {
                    return finish();
                }
                res.write(`id: ${line}\n`);
                res.write(`data: ${text[line++]}\n\n`);
            }, t);
            finish = () => {
                logger.log('Iam done.');
                clearInterval(interval);
                res.end();
            };
            res.on('finish', finish);
            break;
        default:
            res.writeHead(404);
            res.write('Not Found');
            res.end();
            break;
    }
});

server.listen(port, (err) => {
    if (err) {
        return logger.error(`Server failed to start`, err.stack);
    }
    logger.log(`Server is listening on ${port}`);
});	
```

### Client

```html
<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <div id="contents">
        </div>
        <script type="text/javascript">
            function write(para) {
                document.getElementById('contents').innerHTML += '<p>' + para + '</p>';
            }

            (function() {
                if (!window.EventSource) {
                    return write('No SSE :(');
                }
                const address = 'http://localhost:3000/sse';
                const source = new EventSource(address);
                source.addEventListener('message', function(e) {
                    write(e.data);
                }, false);
                source.addEventListener('open', function(e) {
                    write('<b><i> Opened SSE connection to ' + address + '</i><b>');
                }, false);
                source.addEventListener('error', function(e) {
                    if (e.eventPhase == EventSource.CLOSED) {
                        write('<b><i>Connection Closed</i></b>');
                        source.close();
                    }
                }, false);
            })();
        </script>
    </body>
</html>
```

