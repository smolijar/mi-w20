const http = require('http');
const fs = require('fs');

const logger = console;
const port = 3000;
const server = http.createServer();

const text = fs.readFileSync('./text.txt', 'utf8').split('\n');

server.on('request', (req, res) => {

    switch (true) {
        case req.url === '/sse':
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Content-Type', 'text/event-stream');
            res.setHeader('Cache-Control', 'no-cache');
            const t = 2 * 1000;
            let line = 0;
            let finish = null;
            const interval = setInterval(() => {
                if (line === text.length - 1) {
                    return finish();
                }
                res.write(`id: ${line}\n`);
                res.write(`data: ${text[line++]}\n\n`);
            }, t);
            finish = () => {
                logger.log('Iam done.');
                clearInterval(interval);
                res.end();
            };
            res.on('finish', finish);
            break;
        default:
            res.writeHead(404);
            res.write('Not Found');
            res.end();
            break;
    }
});

server.listen(port, (err) => {
    if (err) {
        return logger.error(`Server failed to start`, err.stack);
    }
    logger.log(`Server is listening on ${port}`);
});
