# RESTful - Pagination

## Task

Design and implement simple service - list of orders.

```
GET /orders
```

Assume that this request (at /orders URI) returns large number of orders. It is not appropriate to return the whole list at once. Design and implement this operation with pagination using the Link HTTP header.

Use [http://nodejs.org](http://nodejs.org/) and [http module](http://nodejs.org/api/http.html).

## Solution

Available at [gitlab](https://gitlab.fit.cvut.cz/smolijar/mi-w20/tree/master/Homework/7)

User defines **offset** and **limit** via query string. Links for previous and next page are returned in **Link** header in standard format `<url>; rel="rel"[, ...]`. Total item count is returned in **X-total-count** header.

```javascript
function handle(req, res) {
    const uri = new URI(`http://${req.headers.host}${req.url}`);
    if (uri.path() !== '/customers') {
        return {code: 404};
    }
    if (req.method === 'GET') {
        const query = uri.query(true);
        const limit = +query.limit || customers.length;
        const offset = +query.offset || 0;

        const links = [];
        if(offset > 0) {
            links.push({
                offset: Math.max(0, offset - limit),
                rel: 'prev',
            })
        }
        if(offset + limit < customers.length) {
            links.push({
                offset: Math.min(customers.length, offset + limit),
                rel: 'next',
            })
        }
        headers = {
            Link: links.map((v) => `<${uri.setQuery({offset: v.offset})}>; rel="${v.rel}"`).join(', '),
            'X-total-count': customers.length,
        };
        return {
                code: 200,
                headers,
                content: customers.slice(offset, offset + limit)
            };
    }
    return {code: 405};
}
```

## Example

Starting request with limit only...

```
GET http://localhost:8080/customers?limit=5
```

```
200 OK
Content-Type:application/json
Link:<http://localhost:8080/customers?limit=5&offset=5>; rel="next"
X-total-count:100

[...]
```

Following next link...

```
GET http://localhost:8080/customers?limit=5&offset=5
```

```
200 OK
Content-Type:application/json
Link:<http://localhost:8080/customers?limit=5&offset=0>; rel="prev", <http://localhost:8080/customers?limit=5&offset=10>; rel="next"
X-total-count:100

[...]
```