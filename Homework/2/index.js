const net = require('net');

function advanceProcess(socket, action, process) {
    if (action === 'open' && process === null) {
        process = 0;
        socket.write('Process opened\n');
    }
    else if (action === 'add' && process !== null) {
        socket.write(`Item added to process. Process has ${++process} items.\n`);
    }
    else if (action === 'close' && process !== null) {
        socket.write(`Process closed. Process has ${process} items.\n`);
        socket.end();
        process = null;
    }
    else {
        socket.write(`Invalid operation. Use <open><add>*<close>\n`);
    }
    console.log(action, process);
    return process;
}

const statefulServer = net.createServer((c) => {
    console.log('Socket opened on stateful Server');
    c.setEncoding('utf8');
    c.on('end', () => {
        console.log('Socket closed on stateful Server');
    });
    let process = null;
    c.on('data', (data) => {
        data = data.trim();
        process = advanceProcess(c, data, process);
    });
});

const processes = {};

const statelessServer = net.createServer((c) => {
    console.log('Socket opened on stateless Server');
    c.setEncoding('utf8');
    c.on('end', () => {
        console.log('Socket closed on stateless Server');
    });
    c.on('data', (data) => {
        const [action, pid] = data.trim().split(' ');
        if(processes[pid] === undefined) {
            processes[pid] = null;
        }
        processes[pid] = processes[pid] = advanceProcess(c, action, processes[pid]);
    });
});

statefulServer.listen(8124, function() {
    console.log('statefulServer started on 8125');
});

statelessServer.listen(8125, function() {
    console.log('statelessServer started on 8125');
});