# Simple Statefull/Stateless Server

## Task

Implement simple Statefull (All interactions within one session) and Stateless (Information about state is part of communication) server in [http://nodejs.org](http://nodejs.org/) using [network module](http://nodejs.org/api/net.html).

Your server should be able to communicate using following commmands:

- open - open order
- add - add item
- process - process order

Example of communication (order of commands is important):

```
  -->open (client request)
  <--opened (server response)
  -->add
  <--added
  -->process
  <--processed
```

Do not forget show example of telnet communications.

## Solution

```javascript
const net = require('net');

function advanceProcess(socket, action, process) {
    if (action === 'open' && process === null) {
        process = 0;
        socket.write('Process opened\n');
    }
    else if (action === 'add' && process !== null) {
        socket.write(`Item added to process. Process has ${++process} items.\n`);
    }
    else if (action === 'close' && process !== null) {
        socket.write(`Process closed. Process has ${process} items.\n`);
        socket.end();
        process = null;
    }
    else {
        socket.write(`Invalid operation. Use <open><add>*<close>\n`);
    }
    return process;
}

const statefulServer = net.createServer((c) => {
    console.log('Socket opened on stateful Server');
    c.setEncoding('utf8');
    c.on('end', () => {
        console.log('Socket closed on stateful Server');
    });
    let process = null;
    c.on('data', (data) => {
        data = data.trim();
        process = advanceProcess(c, data, process);
    });
});

const processes = {};

const statelessServer = net.createServer((c) => {
    console.log('Socket opened on stateless Server');
    c.setEncoding('utf8');
    c.on('end', () => {
        console.log('Socket closed on stateless Server');
    });
    c.on('data', (data) => {
        const [action, pid] = data.trim().split(' ');
        if(processes[pid] === undefined) {
            processes[pid] = null;
        }
        processes[pid] = processes[pid] = advanceProcess(c, action, processes[pid]);
    });
});

statefulServer.listen(8124, function() {
    console.log('statefulServer started on 8125');
});

statelessServer.listen(8125, function() {
    console.log('statelessServer started on 8125');
});
```

## Telnet testing

### Stateful

```sh
$ telnet localhost 8124
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
add
Invalid operation. Use <open><add>*<close>
open
Process opened
add
Item added to process. Process has 1 items.
addd
Invalid operation. Use <open><add>*<close>
add
Item added to process. Process has 2 items.
add
Item added to process. Process has 3 items.
add
Item added to process. Process has 4 items.
open
Invalid operation. Use <open><add>*<close>
add
Item added to process. Process has 5 items.
close
Process closed. Process has 5 items.
Connection closed by foreign host.
```

### Stateless

```sh
$ telnet localhost 8125
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
open a
Process opened
add a
Item added to process. Process has 1 items.
add a
Item added to process. Process has 2 items.
add a
Item added to process. Process has 3 items.
open b
Process opened
add b
Item added to process. Process has 1 items.
add b
Item added to process. Process has 2 items.
add x
Invalid operation. Use <open><add>*<close>
close a
Process closed. Process has 3 items.
Connection closed by foreign host.
$ telnet localhost 8125
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
close b
Process closed. Process has 2 items.
Connection closed by foreign host.
```

