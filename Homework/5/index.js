const http = require('http');
const etag = require('etag');

let customers = [
    {
        "id": 1,
        "name": "Gary 1",
        "orders": []
    }
];
let lastMod = new Date();
function growCustomers(id) {
    customers.push({
        id,
        name: `Gary ${id}`,
        orders: []
    });
    lastMod = new Date();
    console.log(`[${lastMod.toUTCString()}] Customer ${id} added`);
    setTimeout(() => {
        growCustomers(id+1);
    }, Math.pow(2, id));
}
growCustomers(2);

function handle(req, res) {
    if (req.url.replace(/^\/|\/$/g, '') !== 'customers') {
        return {code: 404};
    }
    if (req.method === 'GET') {
        strongETag = etag(JSON.stringify(customers));
        weakETag = etag(JSON.stringify(customers.map(c => ({id: c.id, name: c.name}))), {weak: true});
        agentEtags = (req.headers['if-none-match'] || '').split(', ');
        agentDate = new Date(req.headers['if-modified-since'] || 0);

        headers = {
            'ETag': [strongETag, weakETag],
            'Last-Modified': lastMod,
        };

        if(lastMod < agentDate) {
            console.log('304 if modified since');
            return {code: 304, headers}
        }
        if(agentEtags.indexOf(strongETag) !== -1) {
            console.log('304 strong etag');
            return {code: 304, headers}
        }
        if(agentEtags.indexOf(weakETag) !== -1) {
            console.log('304 weak etag');
            return {code: 304, headers}
        }
        console.log('200');
        return {
                code: 200,
                headers,
                content: customers
            };
    }
    return {code: 405};
}

http.createServer((req, res) => {
    req.body = '';
    req.on('data', (chunk) => req.body += chunk);
    req.on('end', () => {
        responseData = handle(req, res);
        switch (responseData.code) {
            case 200:
                responseData.headers = Object.assign({}, responseData.headers, {'Content-Type': 'application/json'});
                responseData.content = JSON.stringify(responseData.content);
                break;
            case 304:
                responseData.content = 'Not Modified';
                break;
            case 400:
                responseData.content = 'Client error';
                break;
            case 404:
                responseData.content = 'Not found';
                break;
            case 405:
                responseData.content = 'Method not allowed';
                break;
        }
        res.writeHead(responseData.code, responseData.headers);
        res.end(responseData.content);
    });
}).listen(8080);