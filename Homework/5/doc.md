# RESTfull - Conditional GET

## Task

Design and implement simple service - get list of customers

**Example:**

```
GET /customers
```

```
[
    {
        "id": 1,
        "name": "aaa",
        "orders": []
    },
    {
        "id": 2,
        "name": "bbb",
        "orders": []
    }
]
```

Implement HTTP caching using **Last-Modified** and **ETag**. Implement two version of ETag: strong and weak ETag. For weak ETag use only the id and the name from each costumer. Do not forget to show examples of communication including all HTTP Headers.

Use [http://nodejs.org](http://nodejs.org/) and [http module](http://nodejs.org/api/http.html).

## Solution

### Implementation, comments

Full implementation available at [gitlab](https://gitlab.fit.cvut.cz/smolijar/mi-w20/tree/master/Homework/5)

#### Dynamic source

Customers are added endlessly in exponentioal steps.

```javascript
function growCustomers(id) {
    customers.push({
        id,
        name: `Gary ${id}`,
        orders: []
    });
    lastMod = new Date();
    console.log(`[${lastMod.toUTCString()}] Customer ${id} added`);
    setTimeout(() => {
        growCustomers(id+1);
    }, Math.pow(2, id));
}
```

#### Last modified

Last modified date is set with each call of `growCustomers`

Then modified date is compared to request header.

```javascript
agentDate = new Date(req.headers['if-modified-since'] || 0); // 1970 if not in header
if(lastMod < agentDate) {
  return {code: 304, headers}
}
// ... else return 200
```

#### ETag

On demand, ETag is created using [etag](https://www.npmjs.com/package/etag).

```javascript
agentEtags = (req.headers['if-none-match'] || '').split(', ');
strongETag = etag(JSON.stringify(customers));
weakETag = etag(JSON.stringify(customers.map(c => ({id: c.id, name: c.name}))), {weak: true});
if(agentEtags.indexOf(strongETag) !== -1 || agentEtags.indexOf(weakETag) !== -1) {
  // req contains either strong or weak ETag
  return {code: 304, headers}
}
// ... else return 200
```

### Example

#### ETag

First I run the server and capture ETag at some state, I am able to determine through server log.

```
GET http://localhost:8080/customers/
```

```
200 OK
Content-Type: application/json
ETag: "1c3-i7y204TAsAnW/CbyEidskGhKABI", W/"133-Xf51EbK1aYUwOR3c5H70DoVnWgI"
Last-Modified: Thu, 23 Mar 2017 14:38:30 GMT

[customers array ... ]
```

```
[Thu, 23 Mar 2017 14:38:28 GMT] Customer 2 added
[Thu, 23 Mar 2017 14:38:28 GMT] Customer 3 added
[Thu, 23 Mar 2017 14:38:28 GMT] Customer 4 added
[Thu, 23 Mar 2017 14:38:28 GMT] Customer 5 added
[Thu, 23 Mar 2017 14:38:28 GMT] Customer 6 added
[Thu, 23 Mar 2017 14:38:28 GMT] Customer 7 added
[Thu, 23 Mar 2017 14:38:28 GMT] Customer 8 added
[Thu, 23 Mar 2017 14:38:28 GMT] Customer 9 added
[Thu, 23 Mar 2017 14:38:29 GMT] Customer 10 added
[Thu, 23 Mar 2017 14:38:30 GMT] Customer 11 added
200
[Thu, 23 Mar 2017 14:38:32 GMT] Customer 12 added
[Thu, 23 Mar 2017 14:38:36 GMT] Customer 13 added
^C
```

Taking ETag from response header, I repeat using it in request to test return code. Example shows usage of weak ETag.

```
GET http://localhost:8080/customers/
If-None-Match: W/"133-Xf51EbK1aYUwOR3c5H70DoVnWgI"
```

```
304 Not Modified
ETag: "1c3-i7y204TAsAnW/CbyEidskGhKABI", W/"133-Xf51EbK1aYUwOR3c5H70DoVnWgI"
Last-Modified: Thu, 23 Mar 2017 14:49:04 GMT
```

```
[Thu, 23 Mar 2017 14:49:02 GMT] Customer 2 added
[Thu, 23 Mar 2017 14:49:02 GMT] Customer 3 added
[Thu, 23 Mar 2017 14:49:02 GMT] Customer 4 added
[Thu, 23 Mar 2017 14:49:02 GMT] Customer 5 added
[Thu, 23 Mar 2017 14:49:02 GMT] Customer 6 added
[Thu, 23 Mar 2017 14:49:02 GMT] Customer 7 added
[Thu, 23 Mar 2017 14:49:02 GMT] Customer 8 added
[Thu, 23 Mar 2017 14:49:02 GMT] Customer 9 added
200
[Thu, 23 Mar 2017 14:49:03 GMT] Customer 10 added
200
[Thu, 23 Mar 2017 14:49:04 GMT] Customer 11 added
304 weak etag
[Thu, 23 Mar 2017 14:49:06 GMT] Customer 12 added
[Thu, 23 Mar 2017 14:49:10 GMT] Customer 13 added
^C
```
#### LastModified

Taking ETag from response header, I repeat using it in request to test return code. Example shows usage of weak ETag.

```
GET http://localhost:8080/customers/
If-Modified-Since: Thu, 23 Mar 2017 14:54:10 GMT
```

```
304 Not Modified
ETag: "25f-LuAV9FFUT6TOvmlE0Wt8JzaypPk", W/"19f-V9FsDomtavlyBnSOlebP3VHVQuE"
Last-Modified: Thu, 23 Mar 2017 14:54:39 GMT

[customers array ... ]
```

```
[Thu, 23 Mar 2017 14:53:34 GMT] Customer 2 added
[Thu, 23 Mar 2017 14:53:34 GMT] Customer 3 added
[Thu, 23 Mar 2017 14:53:34 GMT] Customer 4 added
[Thu, 23 Mar 2017 14:53:34 GMT] Customer 5 added
[Thu, 23 Mar 2017 14:53:34 GMT] Customer 6 added
[Thu, 23 Mar 2017 14:53:34 GMT] Customer 7 added
[Thu, 23 Mar 2017 14:53:34 GMT] Customer 8 added
[Thu, 23 Mar 2017 14:53:34 GMT] Customer 9 added
[Thu, 23 Mar 2017 14:53:35 GMT] Customer 10 added
[Thu, 23 Mar 2017 14:53:36 GMT] Customer 11 added
304 if modified since
[Thu, 23 Mar 2017 14:53:38 GMT] Customer 12 added
304 if modified since
[Thu, 23 Mar 2017 14:53:42 GMT] Customer 13 added
[Thu, 23 Mar 2017 14:53:50 GMT] Customer 14 added
304 if modified since
[Thu, 23 Mar 2017 14:54:06 GMT] Customer 15 added
304 if modified since
[Thu, 23 Mar 2017 14:54:39 GMT] Customer 16 added
200
[Thu, 23 Mar 2017 14:55:45 GMT] Customer 17 added
^C

```