# Data access

## Task

A company needs to design an AJAX aplication that will access various resources on the Web by using JavaScript code running in a browser. This application is not public and only internal employees of the company can use it. This application will be available at the address [http://company.at](http://company.at/). Following table shows a list of URL addresses of resources, their formats and HTTP methods that the application will use to access these resources.

Add all possible access technologies to the table for methods GET and DELETE. Note: parameter {dob} means date of birth.

| Resource                                 | Format | GET  | DELETE |
| ---------------------------------------- | ------ | ---- | ------ |
| [http://company.at/customers](http://company.at/customers) | XML    |      |        |
| [http://company.at/suppliers](http://company.at/suppliers) | JSON   |      |        |
| [http://weather.at/innsbruck](http://weather.at/innsbruck) | XML    |      |        |
| [http://people.at/students](http://people.at/students) | JSON   |      |        |
| [http://people.at/](http://people.at/){dob}/contact | VCARD  |      |        |

## Solution

| Resource                                 | Format | GET                                    | DELETE                                 |
| ---------------------------------------- | ------ | -------------------------------------- | -------------------------------------- |
| [http://company.at/customers](http://company.at/customers) | XML    | AJAX                                   | AJAX                                   |
| [http://company.at/suppliers](http://company.at/suppliers) | JSON   | AJAX, JSONP                            | AJAX, JSONP                            |
| [http://weather.at/innsbruck](http://weather.at/innsbruck) | XML    | AJAX-PROXY* AJAX-CORS**           | AJAX-PROXY* AJAX-CORS**           |
| [http://people.at/students](http://people.at/students) | JSON   | JSONP*** AJAX-PROXY* AJAX-CORS** | JSONP*** AJAX-PROXY* AJAX-CORS** |
| [http://people.at/](http://people.at/){dob}/contact | VCARD  | AJAX-PROXY* AJAX-CORS**           | AJAX-PROXY* AJAX-CORS**           |

* AJAX, being a XHR (XMLHttpRequest), is limited by SOP (*same origin policy*) preventing accessing/manipulating resources from different domain. This restriction only applies for request made from browser as a security measure. Company can create a proxy service at *company.at*. The cross domain call would be requested by *company.at* server, returning the response to the browser, effectively communicating with diffrent domains and not breaking the SOP. Alternatively Company can use public proxy service such as cors-anywhere.herokuapp.com.

** If *people.at* (*weather.at* respectively) allow *people.at* XHRs using CORS, AJAX can be used, for *Origin* HTTP header is sent in XHR. Remote service whitelisting domain *company.at* using *Access-Control-Allow-Origin* HTTP header is mandatory; besides, server must also allow request headers and allowed methods.

*** JSONP is not a XHR and thus is not limited by the SOP.
