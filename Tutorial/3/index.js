const http = require('http');

let orders = [
    {id: 1, customer: "customer_1", item: "item_1"},
    {id: 2, customer: "customer_2", item: "item_2"},
    {id: 3, customer: "customer_3", item: "item_3"},
    {id: 4, customer: "customer_4", item: "item_4"},
    {id: 5, customer: "customer_5", item: "item_5"},
    {id: 6, customer: "customer_6", item: "item_6"},
    {id: 7, customer: "customer_7", item: "item_7"},
    {id: 8, customer: "customer_8", item: "item_8"},
    {id: 9, customer: "customer_9", item: "item_9"},
    {id: 10, customer: "customer_10", item: "item_10"},
    {id: 11, customer: "customer_11", item: "item_11"},
];
const apikeys = ['foo', 'bar'];
function isAuthorized(req) {
    let apikey;
    const query = req.url.split('?')[1];
    if (query) {
        [attr, val] = query.split('=');
        if (attr === 'apikey') {
            apikey = val;
        }
    }
    return apikeys.includes(apikey);
}

function getLinks(orderId, req) {
    let links = [];
    let additional = {};
    links.push({href: `http://${req.headers.host}/orders`, rel: 'list'});
    if(orderId === -1) {
        if(isAuthorized(req)) {
            links.push({href: `http://${req.headers.host}/orders/`, rel: 'create'});
        }
    } else {
        links.push({href: `http://${req.headers.host}/orders/${orderId}`, rel: 'self'});
        if(orderId > 0) {
            links.push({href: `http://${req.headers.host}/orders/${orderId - 1}`, rel: 'prev'});
        }
        if(orderId < orders.length - 1) {
            links.push({href: `http://${req.headers.host}/orders/${orderId + 1}`, rel: 'next'});
        }
        if(isAuthorized(req)) {
            links.push({href: `http://${req.headers.host}/orders/${orderId}`, rel: 'remove'});
        }
    }
    return Object.assign({}, {
        _links: links,
    }, additional)
}

function handle(req, res) {
    const path = req.url.split('?')[0].replace(/^\/|\/$/g, '').split('/');
    const [sourceName, sourceId] = [path[0], +path[1]];
    const orderId = orders.map(v => v.id).indexOf(sourceId);
    if (path.length < 1 || path.length > 2 || sourceName !== 'orders' || (sourceId && orderId === -1)) {
        return {code: 404};
    }
    if (sourceId) {
        if (req.method === 'GET') {
            return {code: 200, content: Object.assign({}, orders[orderId], getLinks(orderId, req))};
        }
        if(!isAuthorized(req)) {
            return {code: 401};
        }
        if (req.method === 'DELETE') {
            orders.splice(orderId, 1);
            return {
                code: 200,
                content: ['Deleted'],
            };
        }
    } else {
        if (req.method === 'GET') {
            return {code: 200, content: Object.assign({}, orders.map(
                (v, i) => {
                    links = [];
                    links.push({href: `http://${req.headers.host}/orders/${i + 1}`, rel: 'detail'});
                    if(isAuthorized(req)) {
                        links.push({href: `http://${req.headers.host}/orders/${i + 1}`, rel: 'remove'});
                    }
                    v._links = links;
                    return v
                }
                ), getLinks(orderId, req))};
        }
        if(!isAuthorized(req)) {
            return {code: 401};
        }
        if (req.method === 'POST') {
            id = orders.reduce((acc, val) => Math.max(acc, val.id), 1) + 1;
            orders.push(Object.assign({}, JSON.parse(req.body), {id}));
            return {
                code: 201,
                headers: {'Location': `http://${req.headers.host}${req.url.split('?')[0]}${id}`}
            };
        }
        return {code: 405};
    }
}

http.createServer((req, res) => {
    req.body = '';
    req.on('data', (chunk) => req.body += chunk);
    req.on('end', () => {
        responseData = handle(req, res);
        switch (responseData.code) {
            case 200:
                responseData.headers = Object.assign({}, responseData.headers, {'Content-Type': 'application/json'});
                responseData.content = JSON.stringify(responseData.content);
                break;
            case 201:
                responseData.content = `New item created at ${responseData.headers.Location}`;
                break;
            case 400:
                responseData.content = 'Client error';
                break;
            case 401:
                responseData.content = 'Unauthorized';
                break;
            case 404:
                responseData.content = 'Not found';
                break;
            case 405:
                responseData.content = 'Method not allowed';
                break;
        }
        res.writeHead(responseData.code, responseData.headers);
        res.end(responseData.content);
    });
}).listen(8080);