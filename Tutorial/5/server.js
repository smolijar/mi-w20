const http = require('http');
const URI = require('urijs');

const messages = [];
const longPolls = [];

function handle(req, res) {
    const uri = new URI(`http://${req.headers.host}${req.url}`);
    if (uri.path() !== '/messages/') {
        return {code: 404};
    }
    if(req.method === 'OPTIONS') {
        returnResponse(res, {code: 200});
    }
    if (req.method === 'GET') {
        switch (req.headers['x-poll']) {
            case 'long':
                longPolls.push(res);
                break;
            default: // simple
                returnResponse(res, {
                    code: 200,
                    content: messages
                });
        }
    }
    else if (req.method === 'POST') {
        const msg = JSON.parse(req.body);
        if(!msg.message || msg.message.trim() === '') {
            returnResponse(res, {
                code: 400,
                content: 'Message must be non-empty',
            });
        }
        messages.push(msg);
        longPolls.forEach(r => {
            returnResponse(r, {code: 200, content: messages});
        });
        returnResponse(res, {
            code: 201,
            headers: {Location: uri.toString()},
            content: messages,
        });
    }
    else {
        return returnResponse(res, {code: 405});
    }
}

function returnResponse(res, responseData) {
    switch (responseData.code) {
        case 200:
            responseData.headers = Object.assign({}, responseData.headers, {'Content-Type': 'application/json'});
            responseData.content = JSON.stringify(responseData.content);
            break;
        case 201:
            responseData.content = 'Resource created';
            break;
        case 404:
            responseData.content = 'Not found';
            break;
        case 405:
            responseData.content = 'Method not allowed';
            break;
    }
    responseData.headers = Object.assign({}, responseData.headers, {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'X-Poll',
        'Access-Control-Allow-Methods': 'GET, POST, OPTIONS'
    });
    res.writeHead(responseData.code, responseData.headers);
    res.end(responseData.content);
}

http.createServer((req, res) => {
    req.body = '';
    req.on('data', (chunk) => req.body += chunk);
    req.on('end', () => {
        handle(req, res);
    });
}).listen(8080);
