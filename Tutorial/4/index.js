const http = require('http');

const customer = [{
    "id": 1,
    "name": "Richard Lawson"
}, {
    "id": 2,
    "name": "Diana Thompson"
}, {
    "id": 3,
    "name": "Earl Nguyen"
}, {
    "id": 4,
    "name": "Christopher Coleman"
}, {
    "id": 5,
    "name": "Angela Simmons"
}];

function handle(req, res) {
    const path = req.url.split('?')[0].replace(/^\/|\/$/g, '');
    headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'content-type',
    };
    if(path === 'service' && req.method === 'GET') {
        const cb = (((req.url.split('?') || ['', ''])[1]) || '').split('=')[1];
        console.log(cb);
        return {
            code: 200,
            headers: Object.assign({}, headers, {
                'Content-Type': 'application/javascript'
            }),
            content: cb ? `${cb}(${JSON.stringify(customer)});` : JSON.stringify(customer),
        };
    }
    if (req.method === 'GET' && path === 'get') {
        return {
                code: 200,
                headers,
                content: 'OK',
            };
    }
    console.log(`ORIGIN ${req.headers.origin}`);
    if(['get', 'put', 'post', 'delete'].indexOf(path) !== -1) {
        if(req.method === 'OPTIONS') {
            console.log(`REQ METHOD ${req.headers['access-control-request-method']}`);
            headers = Object.assign({}, headers, {
                'Access-Control-Allow-Methods': path.toUpperCase(),
            });
            return {
                code: 200,
                headers,
                content: 'OPTIONS OK',
            };
        }
        else if(req.method === path.toUpperCase()) {
            return {
                code: 200,
                headers,
                content: `${req.method} OK`,
            };
        }
        else {
            return {code: 405};
        }
    }
    return {code: 404};
}

http.createServer((req, res) => {
    req.body = '';
    req.on('data', (chunk) => req.body += chunk);
    req.on('end', () => {
        responseData = handle(req, res);
        switch (responseData.code) {
            case 200:
                break;
            case 304:
                responseData.content = 'Not Modified';
                break;
            case 400:
                responseData.content = 'Client error';
                break;
            case 404:
                responseData.content = 'Not found';
                break;
            case 405:
                responseData.content = 'Method not allowed';
                break;
        }
        res.writeHead(responseData.code, responseData.headers);
        res.end(responseData.content);
    });
}).listen(8888);
console.log('Listening');