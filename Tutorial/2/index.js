const http = require("http");


const countries = [
    {id: 'cz', name: "Czech Republic"}
];

const books = [
    {id: 1, isbn: '978-1-4028-9462-1', name: "Book 1", description: 'Description of book 1'},
    {id: 2, isbn: '978-1-4028-9462-2', name: "Book 2", description: 'Description of book 2'},
    {id: 3, isbn: '978-1-4028-9462-3', name: "Book 3", description: 'Description of book 3'},
    {id: 4, isbn: '978-1-4028-9462-4', name: "Book 4", description: 'Description of book 4'},
    {id: 5, isbn: '978-1-4028-9462-5', name: "Book 5", description: 'Description of book 5'},
    {id: 6, isbn: '978-1-4028-9462-6', name: "Book 6", description: 'Description of book 6'},
    {id: 7, isbn: '978-1-4028-9462-7', name: "Book 7", description: 'Description of book 7'},
    {id: 8, isbn: '978-1-4028-9462-8', name: "Book 8", description: 'Description of book 8'},
    {id: 9, isbn: '978-1-4028-9462-9', name: "Book 9", description: 'Description of book 9'},
    {id: 10, isbn: '978-1-4028-9462-0', name: "Book 10", description: 'Description of book 10'},
];

const storage = {books};

function handle(req, res) {
    const path = req.url.substr(1).split('/');
    console.log(path);
    let resourceParent = null;
    let resource = path.reduce((acc, val) => {
        if(acc) {
            resourceParent = acc;
            if(acc[val]) {return acc[val]}
            if(Object.prototype.toString.call( acc ) === '[object Array]') {
                return acc.filter((item) => item.id === val)[0] || undefined;
            }
        }
    }, storage);
    if(resource) {
        if(req.method === 'GET') {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end(JSON.stringify(resource));
        }
        else if (req.method === 'DELETE') {
            storage[path[0]] = resourceParent.filter((r) => r.id != resource.id);
            console.log(resourceParent);
            res.writeHead(200);
            res.end('Deleted');
        }
        else if (req.method === 'POST') {
            id = resource.reduce((acc, val) => Math.max(acc, val.id), 0);
            resource.push(Object.assign({}, JSON.parse(req.body), {id: id + 1}));
            console.log(resourceParent);
            res.writeHead(201, {'Location': `${req.url}/${id}`});
            res.end('Posted');
        }
    } else {
        res.writeHead(404);
        res.end('Not found');
    }
}

http.createServer((req, res) => {
    req.body = "";
    req.on('data', (chunk) => req.body += chunk);
    req.on('end', () => {
        handle(req, res);
    });

}).listen(8080);