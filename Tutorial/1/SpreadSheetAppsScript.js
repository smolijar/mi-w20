function onOpen() {
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    // menu times and function that are run after clicking the menu item
    var menuEntries = [
        {name: "Numerical data", functionName: "numericalData"},
        {name: "Text", functionName: "text"},
        {name: "Map", functionName: "map"},
        {name: "Execute Tasks", functionName: "executeTasks"}, ];
    ss.addMenu("Preview", menuEntries);
}

function numericalData() {
    const data = Browser.inputBox("Numerical data", "Enter numerical data", Browser.Buttons.OK_CANCEL);
    if(data !== 'cancel') {
        const doc = SpreadsheetApp.getActiveSpreadsheet();
        doc.getRange('a1').setValue(data);
        doc.getRange('a4').setValue(data);
    }
}

function text() {
    const data = Browser.inputBox("Text", "Enter text", Browser.Buttons.OK_CANCEL);
    if(data !== 'cancel') {
        const doc = SpreadsheetApp.getActiveSpreadsheet();
        doc.getRange('a2').setValue(data);
    }
}

function map() {
    const data = Browser.inputBox("Map", "Enter a city", Browser.Buttons.OK_CANCEL);
    if(data !== 'cancel') {
        const doc = SpreadsheetApp.getActiveSpreadsheet();
        doc.getRange('a3').setValue(data);
    }
}

function requestUrl(url) {
    var response = UrlFetchApp.fetch(url);
    return JSON.parse(response);
}

function executeTasks() {
    const doc = SpreadsheetApp.getActiveSpreadsheet();
    const a1Eur = doc.getRange('a1').getValue() * 0.0370077582;
    doc.getRange('b1').setValue(a1Eur);

    const translatedA2 = LanguageApp.translate(doc.getRange('a2').getValue(), 'cs', 'en');
    doc.getRange('b2').setValue(translatedA2);

    const city =  doc.getRange('a3').getValue();
    const map = Maps.newStaticMap().setSize(500, 350);
    map.setMarkerStyle(Maps.StaticMap.MarkerSize.MID, "red", null);
    map.addMarker(city);

//  doc.getActiveSheet().insertImage(map.getMapUrl(), 3, 3);
    doc.getRange('c3').setValue(map.getMapUrl());

    const url = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22CZKEUR%22)&format=json&env=store://datatables.org/alltableswithkeys&amp;callback=';
    const rate = requestUrl(url)['query'].results.rate.Rate;
    const a4Val = doc.getRange('a4').getValue();
    doc.getRange('b4').setValue(a4Val * rate);
}
