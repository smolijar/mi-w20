# MI-W20

## Homeworks
1. [Apps Script - RecipePuppy](./Homework/1/doc.md)
2. [Simple Statefull/Stateless Server](./Homework/2/doc.md)
3. [RESTfull - Resources, URIs, operations](./Homework/3/doc.md)
4. [RESTfull - Asynchronous operation](./Homework/4/doc.md)
5. [RESTfull - Conditional GET](./Homework/5/doc.md)
6. [AtomPub](./Homework/6/doc.md)
7. [RESTful - Pagination](./Homework/7/doc.md)
8. [Data access](./Homework/8/doc.md)
9. [Server-Sent Events](./Homework/9/doc.md)
10. [Microdata Course](./Homework/10/doc.md)
11. [RDF - "Bonus"](./Homework/11/doc.md)











